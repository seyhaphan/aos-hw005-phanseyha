package com.kshrd.homework5.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.kshrd.homework5.R;
import com.kshrd.homework5.ReadActivity;
import com.kshrd.homework5.helpers.UriConverters;
import com.kshrd.homework5.room.AppDatabase;
import com.kshrd.homework5.room.entity.Book;
import com.kshrd.homework5.room.entity.BookResponse;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.MyViewHolder>{
    Context context;
    List<BookResponse> bookResponses;
    AppDatabase instance;

    private static final int PICK_IMAGE_REQ = 100;
    private Uri imageUri;
    private ImageView imageView ;
    private Spinner categorySpinner;
    private int categoryId;

    public BookAdapter(Context context, List<BookResponse> bookResponses) {
        this.context = context;
        this.bookResponses = bookResponses;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        instance = AppDatabase.getAppDatabase(context);
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_layout, parent, false);
        return new MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Glide.with(context.getApplicationContext())
                .load(bookResponses.get(position).getImageUrl())
                .into(holder.imageView);

        holder.tvTitle.setText(bookResponses.get(position).getTitle());
        holder.tvSize.setText(bookResponses.get(position).getSize()+"");
        holder.tvPrice.setText(bookResponses.get(position).getPrice()+"");
        holder.tvCategoryName.setText(bookResponses.get(position).getCategoryName());

        // more option click
        holder.moreOption.setOnClickListener(v -> {

            PopupMenu popupMenu = new PopupMenu(context, v);
            MenuInflater menuInflater = popupMenu.getMenuInflater();
            menuInflater.inflate(R.menu.popup_menu,popupMenu.getMenu());
            popupMenu.show();

            popupMenu.setOnMenuItemClickListener(item -> {
                switch (item.getItemId()){
                    case R.id.btn_edit:
                        inputForm(position);
                        return true;

                    case R.id.btnRemove:
                        instance.bookDao().deleteBook(bookResponses.get(position).getBookId());
                        Toast.makeText(context,"Deleted Successfully",Toast.LENGTH_SHORT).show();
                        return true;
                    case R.id.btnRead:
                        Intent intent = new Intent(context.getApplicationContext(), ReadActivity.class);
                        intent.putExtra("DATA",bookResponses.get(position));
                        context.startActivity(intent);
                        return true;
                    default:
                        return false;
                }
            });

        });
    }


    @Override
    public int getItemCount() {
        return bookResponses.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView,moreOption;
        TextView tvTitle,tvPrice,tvSize,tvCategoryName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.home_imageView);
            moreOption = itemView.findViewById(R.id.moreOption);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvPrice = itemView.findViewById(R.id.tvPrice);
            tvSize = itemView.findViewById(R.id.tvSize);
            tvCategoryName = itemView.findViewById(R.id.tvCategory);
        }
    }

    public void inputForm(int position){
        Dialog customDialog;
        customDialog = new Dialog(context);
        customDialog.setContentView(R.layout.form_layout);

        imageView = customDialog.findViewById(R.id.imageView);
        categorySpinner = customDialog.findViewById(R.id.category);
        TextView tvTitle = customDialog.findViewById(R.id.tvTitle);
        EditText edTitle = customDialog.findViewById(R.id.edTitle);
        EditText edSize = customDialog.findViewById(R.id.edSize);
        EditText edPrice = customDialog.findViewById(R.id.edPrice);
        Button btnSave = customDialog.findViewById(R.id.btnSave);
        Button btnCancel = customDialog.findViewById(R.id.btnCancel);

        List<String> categories = instance.categoryDao().findAll();

        ArrayAdapter adapterCategory = new ArrayAdapter(context, android.R.layout.simple_spinner_item,categories);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        categorySpinner.setAdapter(adapterCategory);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categoryId = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        edTitle.setText(bookResponses.get(position).getTitle());
        edSize.setText(bookResponses.get(position).getSize()+"");
        edPrice.setText(bookResponses.get(position).getPrice()+"");
        Glide.with(context).load(bookResponses.get(position).getImageUrl())
                .into(imageView);
        imageUri = UriConverters.fromString(bookResponses.get(position).getImageUrl());

        if(bookResponses.get(position).getCategoryName().equals("Sale")){
           categorySpinner.setSelection(0);
        }else{
           categorySpinner.setSelection(1);
        }

        customDialog.show();


        //cancel dialog
        btnCancel.setOnClickListener(v -> {
            customDialog.dismiss();
        });

        tvTitle.setText("Update Book");
        btnSave.setText("Update");

        //update data
        btnSave.setOnClickListener(v -> {

            Book book = new Book();
            book.setTitle(edTitle.getText().toString());
            book.setCategoryId(categoryId+1);
            book.setSize(Long.parseLong(edSize.getText().toString()));
            book.setPrice(Double.parseDouble(edPrice.getText().toString()));
            book.setImageUrl(imageUri);

            Log.d("MainActivity", "onItemSelected: "+book.toString());

            instance.bookDao().updateBook(book);
            customDialog.dismiss();

            Toast.makeText(context,"Updated successfully",Toast.LENGTH_SHORT).show();

        });

        //pick image from gallery
        customDialog.findViewById(R.id.btnPickImage).setOnClickListener(v -> {

            openGallery();

        });
    }

    private void openGallery(){
        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        context.startActivity(pickIntent);
    }


}
