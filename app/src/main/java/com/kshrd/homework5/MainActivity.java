package com.kshrd.homework5;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.kshrd.homework5.adapter.BookAdapter;
import com.kshrd.homework5.fragments.DashboardFragment;
import com.kshrd.homework5.fragments.HomeFragment;
import com.kshrd.homework5.fragments.NotificationFragment;
import com.kshrd.homework5.fragments.ShareFragment;
import com.kshrd.homework5.fragments.UploadFragment;
import com.kshrd.homework5.room.AppDatabase;
import com.kshrd.homework5.room.entity.Book;
import com.kshrd.homework5.room.entity.BookResponse;
import com.kshrd.homework5.room.entity.Category;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private AppDatabase instance;
    private BottomNavigationView bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomNavigation = findViewById(R.id.bottom_navigation);
        instance = AppDatabase.getAppDatabase(this);

        //init category
        if(instance.categoryDao().findAll().size() == 0){
            instance.categoryDao().insert(new Category("Sale"));
            instance.categoryDao().insert(new Category("Donate"));
        }

        //add default fragment
        getSupportActionBar().setTitle("Home");
        getSupportFragmentManager().beginTransaction()
                .add(R.id.container,new HomeFragment())
                .commit();

        //change bottom navigation replace fragment
        bottomNavigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()){
                case R.id.btn_home:
                    getSupportActionBar().setTitle(item.getTitle());
                    replaceFragment(new HomeFragment());
                    return true;
                case R.id.btn_dashboard:
                    getSupportActionBar().setTitle(item.getTitle());
                    replaceFragment(new DashboardFragment());
                    return true;
                case R.id.btn_notification:
                    getSupportActionBar().setTitle(item.getTitle());
                    replaceFragment(new NotificationFragment());
                    return true;
                case R.id.btn_upload:
                    getSupportActionBar().setTitle(item.getTitle());
                    replaceFragment(new UploadFragment());
                    return true;
                case R.id.btn_share:
                    getSupportActionBar().setTitle(item.getTitle());
                    replaceFragment(new ShareFragment());
                    return true;
            }
            return true;
        });
    }

    private void replaceFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container,fragment)
                .addToBackStack(null)
                .commit();
    }

}