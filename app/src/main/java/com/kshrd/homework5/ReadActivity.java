package com.kshrd.homework5;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kshrd.homework5.helpers.UriConverters;
import com.kshrd.homework5.room.entity.BookResponse;

public class ReadActivity extends AppCompatActivity {
    private Button btnBack;
    private ImageView imageView;
    private TextView tvTitle,tvCategory,tvSize,tvPrice;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read);

        btnBack = findViewById(R.id.btnBack);
        imageView = findViewById(R.id.imageView);
        tvTitle = findViewById(R.id.tvTitle);
        tvSize = findViewById(R.id.tvSize);
        tvPrice = findViewById(R.id.tvPrice);


        Intent intent = getIntent();
        BookResponse bookResponse = (BookResponse) intent.getSerializableExtra("DATA");

        Glide.with(this)
                .load(bookResponse.getImageUrl())
                .into(imageView);
        tvTitle.setText(bookResponse.getTitle());
        tvSize.setText(bookResponse.getSize()+"");
        tvPrice.setText(bookResponse.getPrice()+"");

        btnBack.setOnClickListener(v -> {
            finish();
        });
    }

}