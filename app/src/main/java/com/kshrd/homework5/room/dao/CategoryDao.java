package com.kshrd.homework5.room.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.kshrd.homework5.room.entity.Category;

import java.util.List;

@Dao
public interface CategoryDao {
    @Insert
    void insert(Category category);

    @Query("select categoryName from category")
    List<String> findAll();
}
