package com.kshrd.homework5.room.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.kshrd.homework5.room.entity.Book;
import com.kshrd.homework5.room.entity.BookResponse;

import java.util.List;

@Dao
public interface BookDao {

    @Query("select * from book inner join category where book.categoryId = category.categoryId")
    LiveData<List<BookResponse>> findAll();

    @Insert
    void insertBook(Book book);

    @Query("delete from book where bookId = :bookId")
    void deleteBook(int bookId);

    @Update
    void updateBook(Book book);
}
