package com.kshrd.homework5.room;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.kshrd.homework5.helpers.UriConverters;
import com.kshrd.homework5.room.dao.BookDao;
import com.kshrd.homework5.room.dao.CategoryDao;
import com.kshrd.homework5.room.entity.Book;
import com.kshrd.homework5.room.entity.Category;

@Database(entities = {Book.class, Category.class},version = 1,exportSchema = false)
@TypeConverters({UriConverters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract BookDao bookDao();
    public abstract CategoryDao categoryDao();
    private static AppDatabase instance;
    public static AppDatabase getAppDatabase(Context context) {
        if(instance == null){
            synchronized (AppDatabase.class){
                if(instance == null){
                    instance = Room.databaseBuilder(
                            context.getApplicationContext(),
                            AppDatabase.class,"my_db")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return instance;
    }
}
