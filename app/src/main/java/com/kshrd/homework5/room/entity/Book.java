package com.kshrd.homework5.room.entity;

import android.net.Uri;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverter;

@Entity(tableName = "book")
public class Book {
    @PrimaryKey(autoGenerate = true)
    private int bookId;

    @ForeignKey(
            entity = Category.class,
            parentColumns = "categoryId",
            childColumns = "categoryId"
            )
    private int categoryId;
    private String title;
    private long size;
    private double price;
    private Uri imageUrl;

    public Book() {
    }

    public Book(int categoryId, String title, long size, double price, Uri imageUrl) {
        this.categoryId = categoryId;
        this.title = title;
        this.size = size;
        this.price = price;
        this.imageUrl = imageUrl;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Uri getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(Uri imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Book{" +
                "bookId=" + bookId +
                ", categoryId=" + categoryId +
                ", title='" + title + '\'' +
                ", size=" + size +
                ", price=" + price +
                ", imageUrl='" + imageUrl + '\'' +
                '}';
    }
}
