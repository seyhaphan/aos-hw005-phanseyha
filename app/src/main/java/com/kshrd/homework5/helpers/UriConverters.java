package com.kshrd.homework5.helpers;

import android.net.Uri;

import androidx.room.TypeConverter;

public class UriConverters {
    @TypeConverter
    public static Uri fromString(String value){
        return value == null ? null : Uri.parse(value);
    }

    @TypeConverter
    public static String toString(Uri uri){
        return uri.toString();
    }

}
