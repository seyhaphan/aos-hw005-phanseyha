package com.kshrd.homework5.fragments;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.kshrd.homework5.R;
import com.kshrd.homework5.adapter.BookAdapter;
import com.kshrd.homework5.helpers.*;
import com.kshrd.homework5.room.AppDatabase;
import com.kshrd.homework5.room.entity.Book;
import com.kshrd.homework5.room.entity.BookResponse;

import java.util.List;

public class HomeFragment extends Fragment{

    private static final int PICK_IMAGE_REQ = 100;
    private Uri imageUri;
    private ImageView imageView ;
    private Spinner categorySpinner;
    private int categoryId;

    private RecyclerView recyclerView;
    private LiveData<List<BookResponse>> bookResponses;
    private TextView textDefault;

    private AppDatabase instance = AppDatabase.getAppDatabase(getContext());;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        textDefault =  view.findViewById(R.id.defaultText);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.myRecyclerView);

        bookResponses = instance.bookDao().findAll();
        recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));

        bookResponses.observe(this, new Observer<List<BookResponse>>() {
            @Override
            public void onChanged(List<BookResponse> bookResponses) {

                recyclerView.setAdapter(new BookAdapter(getContext(),bookResponses));

                if(bookResponses.size() > 0){
                    textDefault.setVisibility(View.GONE);
                }else{
                    textDefault.setVisibility(View.VISIBLE);
                }

            }
        });

    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.top_menu,menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.btn_add:
                inputForm();
                return true;
        }

        return false;
    }

    public void inputForm(){
        Dialog customDialog;
        customDialog = new Dialog(getContext());
        customDialog.setContentView(R.layout.form_layout);

        imageView = customDialog.findViewById(R.id.imageView);
        categorySpinner = customDialog.findViewById(R.id.category);

        EditText edTitle = customDialog.findViewById(R.id.edTitle);
        EditText edSize = customDialog.findViewById(R.id.edSize);
        EditText edPrice = customDialog.findViewById(R.id.edPrice);
        Button btnSave = customDialog.findViewById(R.id.btnSave);
        Button btnCancel = customDialog.findViewById(R.id.btnCancel);

        List<String> categories = instance.categoryDao().findAll();

        ArrayAdapter adapterCategory = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item,categories);
        adapterCategory.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        categorySpinner.setAdapter(adapterCategory);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categoryId = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        customDialog.show();


        //cancel dialog
        btnCancel.setOnClickListener(v -> {
            customDialog.dismiss();
        });


        //save data
        btnSave.setOnClickListener(v -> {

            Book book = new Book();
            book.setTitle(edTitle.getText().toString());
            book.setCategoryId(categoryId+1);
            book.setSize(Long.parseLong(edSize.getText().toString()));
            book.setPrice(Double.parseDouble(edPrice.getText().toString()));
            book.setImageUrl(imageUri);
            Log.d("MainActivity", "onItemSelected: "+book.getCategoryId());
            instance.bookDao().insertBook(book);
            customDialog.dismiss();

            Toast.makeText(getContext(),"inserted successfully",Toast.LENGTH_SHORT).show();

        });

        //pick image from gallery
        customDialog.findViewById(R.id.btnPickImage).setOnClickListener(v -> {
            if(checkPermission()){
                openGallery();
            }
        });
    }

    private void openGallery(){
        Intent pickIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
        startActivityForResult(pickIntent,PICK_IMAGE_REQ);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == PICK_IMAGE_REQ && data != null){
            imageUri = data.getData();
            imageView.setImageURI(data.getData());
        }
    }


    private boolean checkPermission(){
        if(ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){
            return true;
        }else{
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},101);
            return false;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d("MainActivity", "onRequestPermissionsResult: "+ grantResults[0]);
        if(requestCode == 101){
            if(grantResults.length> 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                openGallery();
            }else{
                Toast.makeText(getContext(),"Permission denied...",Toast.LENGTH_SHORT).show();
            }
        }

    }

}